import { socketConnection, sendNewMessage, sendClear } from "./js/socket.js"
import { changeVisibility } from "./js/workWithElements.js"
import { getColor } from "./js/additionalFunctions.js"

socketConnection();

const btnChooseUser = document.querySelector('#btnChooseUser');

export const inputMessage = document.querySelector('#textMessage');
const btnSend = document.querySelector('#btnSend');

const btnClear = document.querySelector('#btnClear');

btnClear.addEventListener('click', sendClear);

btnSend.addEventListener('click', (event) => {
    let textValue = inputMessage.value;
    sendNewMessage(textValue);
});

btnChooseUser.addEventListener('click', changeVisibility);
btnChooseUser.addEventListener('click', getColor)


inputMessage.addEventListener('keydown', (event) => {
    let textValue = inputMessage.value;
    if(event.keyCode === 13) {
        sendNewMessage(textValue);
        // inputMessage.value = '';
    }
    
});
