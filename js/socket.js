import { addDOM, clearDOM } from "./workWithElements.js"
import { userName } from "./workWithElements.js"
import { colorUser } from "./additionalFunctions.js"
import { inputMessage } from "../chat.js"
let socket;

export function socketConnection() {
    socket = new WebSocket("ws://localhost:8081");

    socket.onopen = function (event) {
        const message = JSON.stringify({ type: "onload" });
        socket.send(message); 
    };
    
    socket.onmessage = function(event) {
        const { type, data } = JSON.parse(event.data);
        switch (type) {
            case "allMessages": {
                data.forEach((element) => {
                    addDOM(element);
                });
                break;
            }
      
            case "message": {
                addDOM(data);
                break;
            }
      
            case "clear": {
                clearDOM();
                break;
            }
      
            default:
                break;
            }
    };
}

export function sendNewMessage(messageValue) {
    let message;
    message = JSON.stringify(
        {
            type: "message",
            data: {user: userName, message: messageValue, color: colorUser}
        }
    )
    socket.send(message);
    inputMessage.value = ''; 
}

export function sendClear() {
    const message = JSON.stringify({
        type: "clear",
        data: {},
      });
      socket.send(message);
}