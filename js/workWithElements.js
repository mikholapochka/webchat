//import { objUsersColors } from "./additionalFunctions.js"

const inputUser = document.querySelector('#user');

export let userName;

const selectUser = document.querySelector('.selectUser');
const chatMessages = document.querySelector('.chatMessages');
const divMessages = document.querySelector('.messages');

export function addDOM(item) {
  let divMessage = document.createElement("div");
  divMessage.className += `${item.user} ${item.color}`;
  divMessage.textContent = `${item.user}: ${item.message}`;
  chatMessages.append(divMessage);

  if (inputUser.value === item.user) {
    // changeMessageColor(item.user);
    divMessage.style.alignSelf = "flex-end";
  }
}

function changeMessageColor(user) {
  let elements = document.getElementsByClassName(user);
  let array = Array.from(elements).forEach((elem) => {
    if (!elem.value) {
      elem.style.alignSelf = "flex-end";
    }
  });
}

export function changeVisibility() {
  userName = inputUser.value;
  // changeMessageColor(userName);
  let h3 = document.querySelector(".userName");
  h3.textContent = userName;
  h3.style.visibility = "visible";
  selectUser.style.display = "none";
  divMessages.style.display = "block";
  btnClear.style.visibility = "visible";
}

export function clearDOM() {
  chatMessages.innerHTML = ``;
  // Object.keys(objUsersColors).forEach(function (key) {
  //   delete objUsersColors[key];
  // });
  
}
